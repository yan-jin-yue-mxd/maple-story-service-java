package constants.string;

import java.nio.charset.Charset;

public class CharsetConstants {
    public static final Charset CURRENT_CHARSET = Charset.forName(MapleLanguageType.LANGUAGE_ZH_CN.getAscii());

    public enum MapleLanguageType {
        LANGUAGE_PT_BR(1, "ISO-8859-1"),
        LANGUAGE_US(2, "US-ASCII"),
        LANGUAGE_ZH_CN(3, "GBK");
        final byte type;
        final String ascii;

        MapleLanguageType(int type, String ascii) {
            this.type = (byte) type;
            this.ascii = ascii;
        }

        public String getAscii() {
            return ascii;
        }

        public byte getType() {
            return type;
        }

        public static MapleLanguageType getByType(byte type) {
            for (MapleLanguageType l : MapleLanguageType.values()) {
                if (l.getType() == type) {
                    return l;
                }
            }
            return LANGUAGE_PT_BR;
        }
    }
}