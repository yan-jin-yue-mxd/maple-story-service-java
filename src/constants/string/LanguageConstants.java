package constants.string;

import client.MapleCharacter;

/**
 * 语言常量
 * @author Drago (Dragohe4rt)
 * @UpdatedBy Yan
 */
public class LanguageConstants {
    
    enum Language {
        /**
         * 葡萄牙语
         */
        LANG_PRT(0),
        /**
         * esp:西班牙文:000A
         */
        LANG_ESP(1),
        /**
         * eng:国际英文:0809
         */
        LANG_ENG(2),

        /**
         * 简体中文
         */
        LANG_ZH_CN(3);
        
        int lang;
        
        private Language(int lang) {
            this.lang = lang;
        }
        
        private int getValue() {
            return this.lang;
        }
        
    }
    
    public static String CPQBlue[] = new String[3];
    public static String CPQError[] = new String[3];
    public static String CPQEntry[] = new String[3];
    public static String CPQFindError[] = new String[3];
    public static String CPQRed[] = new String[3];
    public static String CPQPlayerExit[] = new String[3];
    public static String CPQEntryLobby[] = new String[3];
    public static String CPQPickRoom[] = new String[3];
    public static String CPQExtendTime[] = new String[3];
    public static String CPQLeaderNotFound[] = new String[3];
    public static String CPQChallengeRoomAnswer[] = new String[3];
    public static String CPQChallengeRoomSent[] = new String[3];
    public static String CPQChallengeRoomDenied[] = new String[3];
    
    static {
        int lang;
        
        lang = Language.LANG_PRT.getValue();
        LanguageConstants.CPQBlue[lang] = "Maple Azul";
        LanguageConstants.CPQRed[lang] = "Maple Vermelho";
        LanguageConstants.CPQExtendTime[lang] = "O tempo foi estendido.";
        LanguageConstants.CPQPlayerExit[lang] = " deixou o Carnaval de Monstros.";
        LanguageConstants.CPQError[lang] = "Ocorreu um problema. Favor recriar a sala.";
        LanguageConstants.CPQLeaderNotFound[lang] = "Nao foi possivel encontrar o Lider.";
        LanguageConstants.CPQPickRoom[lang] = "Inscreva-se no Festival de Monstros!\r\n";            
        LanguageConstants.CPQChallengeRoomAnswer[lang] = "O grupo esta respondendo um desafio no momento.";
        LanguageConstants.CPQChallengeRoomSent[lang] = "Um desafio foi enviado para o grupo na sala. Aguarde um momento.";
        LanguageConstants.CPQChallengeRoomDenied[lang] = "O grupo na sala cancelou seu desafio.";
        LanguageConstants.CPQFindError[lang] = "Nao foi possivel encontrar um grupo nesta sala.\r\nProvavelmente o grupo foi desfeito dentro da sala!";
        LanguageConstants.CPQEntryLobby[lang] = "Agora voce ira receber desafios de outros grupos. Se voce nao aceitar um desafio em 3 minutos, voce sera levado para fora.";
        LanguageConstants.CPQEntry[lang] = "Voce pode selecionar \"Invocar Monstros\", \"Habilidade\", ou \"Protetor\" como sua tatica durante o Carnaval dos Monstros. Use Tab a F1~F12 para acesso rapido!";

        lang = Language.LANG_ESP.getValue();
        LanguageConstants.CPQBlue[lang] = "Maple Azul";
        LanguageConstants.CPQRed[lang] = "Maple Rojo";
        LanguageConstants.CPQExtendTime[lang] = "El tiempo se ha ampliado.";
        LanguageConstants.CPQPlayerExit[lang] = " ha dejado el Carnaval de Monstruos.";
        LanguageConstants.CPQLeaderNotFound[lang] = "No se pudo encontrar el Lider.";
        LanguageConstants.CPQPickRoom[lang] = "!Inscribete en el Festival de Monstruos!\r\n";
        LanguageConstants.CPQError[lang] = "Se ha producido un problema. Por favor, volver a crear una sala.";
        LanguageConstants.CPQChallengeRoomAnswer[lang] = "El grupo esta respondiendo un desafio en el momento.";
        LanguageConstants.CPQChallengeRoomSent[lang] = "Un desafio fue enviado al grupo en la sala. Espera un momento.";
        LanguageConstants.CPQChallengeRoomDenied[lang] = "El grupo en la sala cancelo su desafio.";
        LanguageConstants.CPQFindError[lang] = "No se pudo encontrar un grupo en esta sala.\r\nProbablemente el grupo fue deshecho dentro de la sala!";
        LanguageConstants.CPQEntryLobby[lang] = "Ahora usted recibira los retos de otros grupos. Si usted no acepta un desafio en 3 minutos, usted sera llevado hacia fuera.";
        LanguageConstants.CPQEntry[lang] = "Usted puede seleccionar \"Invocar Monstruos\", \"Habilidad\", o \"Protector\" como su tactica durante el Carnaval de los Monstruos. Utilice Tab y F1 ~ F12 para acceso rapido!";
        
        lang = Language.LANG_ENG.getValue();
        LanguageConstants.CPQBlue[lang] = "Maple Blue";
        LanguageConstants.CPQRed[lang] = "Maple Red";
        LanguageConstants.CPQPlayerExit[lang] = " left the Carnival of Monsters.";
        LanguageConstants.CPQExtendTime[lang] = "The time has been extended.";
        LanguageConstants.CPQLeaderNotFound[lang] = "Could not find the Leader.";
        LanguageConstants.CPQError[lang] = "There was a problem. Please re-create a room.";
        LanguageConstants.CPQPickRoom[lang] = "Sign up for the Monster Festival!\r\n";
        LanguageConstants.CPQChallengeRoomAnswer[lang] = "The group is currently facing a challenge.";
        LanguageConstants.CPQChallengeRoomSent[lang] = "A challenge has been sent to the group in the room. Please wait a while.";
        LanguageConstants.CPQChallengeRoomDenied[lang] = "The group in the room canceled your challenge.";
        LanguageConstants.CPQFindError[lang] = "We could not find a group in this room.\r\nProbably the group was scrapped inside the room!";
        LanguageConstants.CPQEntryLobby[lang] = "You will now receive challenges from other groups. If you do not accept a challenge within 3 minutes, you will be taken out.";
        LanguageConstants.CPQEntry[lang] = "You can select \"Summon Monsters\", \"Ability\", or \"Protector\" as your tactic during the Monster Carnival. Use Tab and F1 ~ F12 for quick access!";

        lang = Language.LANG_ZH_CN.getValue();
        LanguageConstants.CPQBlue[lang] = "蓝色";
        LanguageConstants.CPQRed[lang] = "红色";
        LanguageConstants.CPQPlayerExit[lang] = "角色退出怪物嘉年华";
        LanguageConstants.CPQExtendTime[lang] = "已经延长时间";
        LanguageConstants.CPQLeaderNotFound[lang] = "找不到队长";
        LanguageConstants.CPQError[lang] = "出现未知错误，请重新创建房间";
        LanguageConstants.CPQPickRoom[lang] = "参加怪物嘉年华\r\n";
        LanguageConstants.CPQChallengeRoomAnswer[lang] = "当前队伍正在被挑战.";
        LanguageConstants.CPQChallengeRoomSent[lang] = "向房间内的队伍发送了一个挑战。请稍候";
        LanguageConstants.CPQChallengeRoomDenied[lang] = "房间里的队伍取消了你的挑战";
        LanguageConstants.CPQFindError[lang] = "房间里面没有队伍.\r\n也许队伍已经被淘汰了!";
        LanguageConstants.CPQEntryLobby[lang] = "您现在将收到来自其他队伍的挑战。如果3分钟内不接受挑战，你将被淘汰。";
        LanguageConstants.CPQEntry[lang] = "在怪物嘉年华期间，你可以选择\"召唤怪物\"、\"能力\"或\"保护者\"作为你的战术。使用Tab和F1~F12快速访问";
        
    }

    public static String getMessage(MapleCharacter chr, String[] message) {
        return message[chr.getLanguage()];
    }
}
