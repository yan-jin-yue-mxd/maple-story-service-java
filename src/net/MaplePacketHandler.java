/*
	This file is part of the OdinMS Maple Story Server
    Copyright (C) 2008 Patrick Huy <patrick.huy@frz.cc>
		       Matthias Butz <matze@odinms.de>
		       Jan Christian Meyer <vimes@odinms.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation version 3 as published by
    the Free Software Foundation. You may not use, modify or distribute
    this program under any other version of the GNU Affero General Public
    License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package net;

import client.MapleClient;
import tools.data.input.SeekableLittleEndianAccessor;

/**
 * 数据包处理接口
 */
public interface MaplePacketHandler {
    /**
     * 处理数据包
     * @param slea 可以索引的小端访问器
     * @param c 客户端对象
     */
    void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c);
    boolean validateState(MapleClient c);
}
