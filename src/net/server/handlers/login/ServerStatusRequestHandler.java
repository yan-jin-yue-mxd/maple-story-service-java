package net.server.handlers.login;

import client.MapleClient;
import net.AbstractMaplePacketHandler;
import net.server.world.World;
import net.server.Server;
import tools.MaplePacketCreator;
import tools.data.input.SeekableLittleEndianAccessor;

/**
 * 请求服务器状态处理器
 * 选区后会请求服务器状态
 */
public final class ServerStatusRequestHandler extends AbstractMaplePacketHandler {

    @Override
    public void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c) {
        byte worldId = (byte) slea.readShort();
        World world = Server.getInstance().getWorld(worldId);
        if(world != null) {
            int status = world.getWorldCapacityStatus();
            c.announce(MaplePacketCreator.getServerStatus(status));
        } else {
            c.announce(MaplePacketCreator.getServerStatus(2));
        }
    }
}
