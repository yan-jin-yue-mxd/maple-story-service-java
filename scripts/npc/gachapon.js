/*
	This file is part of the OdinMS Maple Story Server
    Copyright (C) 2008 Patrick Huy <patrick.huy@frz.cc>
		       Matthias Butz <matze@odinms.de>
		       Jan Christian Meyer <vimes@odinms.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation version 3 as published by
    the Free Software Foundation. You may not use, modify or distribute
    this program under any other version of the GNU Affero General Public
    License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/* NPC Base
	Map Name (Map ID)
	Extra NPC info.
 */
/*
var status;
var ticketId = 5220000;
var mapName = ["Henesys", "Ellinia", "Perion", "Kerning City", "Sleepywood", "Mushroom Shrine", "Showa Spa (M)", "Showa Spa (F)", "Ludibrium", "New Leaf City", "El Nath", "Nautilus"];
var curMapName = "";

function start() {
    status = -1;
	curMapName = mapName[(cm.getNpc() != 9100117 && cm.getNpc() != 9100109) ? (cm.getNpc() - 9100100) : cm.getNpc() == 9100109 ? 9 : 11];

    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (mode < 0)
        cm.dispose();
    else {
        if (mode == 1)
            status++;
        else
            status--;
        if (status == 0 && mode == 1) {
			if (cm.haveItem(ticketId)) {
				cm.sendYesNo("You may use the " + curMapName + " Gachapon. Would you like to use your Gachapon ticket?");
			} else {
				cm.sendSimple("Welcome to the " + curMapName + " Gachapon. How may I help you?\r\n\r\n#L0#What is Gachapon?#l\r\n#L1#Where can you buy Gachapon tickets?#l");
			}
		} else if(status == 1 && cm.haveItem(ticketId)) {
			if(cm.canHold(1302000) && cm.canHold(2000000) && cm.canHold(3010001) && cm.canHold(4000000)) { // One free slot in every inventory.
				cm.gainItem(ticketId, -1);
				cm.doGachapon();
			} else {
				cm.sendOk("Please have at least one slot in your #rEQUIP, USE, SET-UP, #kand #rETC#k inventories free.");
			}
			cm.dispose();
		} else if(status == 1) {
			if (selection == 0) {
                cm.sendNext("Play Gachapon to earn rare scrolls, equipment, chairs, mastery books, and other cool items! All you need is a #bGachapon Ticket#k to be the winner of a random mix of items.");
            } else {
                cm.sendNext("Gachapon Tickets are available in the #rCash Shop#k and can be purchased using NX or Maple Points. Click on the red SHOP at the lower right hand corner of the screen to visit the #rCash Shop#k where you can purchase tickets.");
            }
		} else if(status == 2) {
			cm.sendNextPrev("You'll find a variety of items from the " + curMapName + " Gachapon, but you'll most likely find items and scrolls related to " + curMapName + ".");
		} else {
			cm.dispose();
		}
    }
}
*/
// =========================================================

// 记录对话交互的状态
var step = -1;
// 标记对话框是最后一页
var end = false;
// 百宝券
var ticketId = 5220000;
// 地图集合
// var mapName = ["射手村"];
var mapName = ["射手村", "Ellinia", "Perion", "Kerning City", "Sleepywood", "Mushroom Shrine", "Showa Spa (M)", "Showa Spa (F)", "Ludibrium", "New Leaf City", "El Nath", "Nautilus"];
// 当前地图名称
var curMapName = "";
// 对话框信息
var message = "";

/**
 * 对话框打开时被回调
 */
function start() {
    // 打开对话框时，初始化步数为 0
    step = 0;
    // 记录当前地图名称
    // 9100100 ~ 9100111：都是 Gachapon(百宝箱)
    // 9100117 : 也是百宝箱
    // 根据 npc 时哪个地图的百宝箱来判断当前处于哪个地图
    curMapName = mapName[(cm.getNpc() != 9100117 && cm.getNpc() != 9100109) ? (cm.getNpc() - 9100100) : cm.getNpc() == 9100109 ? 9 : 11];
    // 本MS删掉了除射手村以外的百宝箱，所以这里永远是射手村地图
    // curMapName = mapName[0];
    action(1, 0, 0);
}

/**
 * 和 NPC有交互时，action 方法会被服务器src/scripting/npc/NPCScriptManager.java#action回调，包括点击[结束对话]时也会被回调,直到调用 cm.dispose()主动释放对话才真正结束了对话。
 * @param mode 根据 type 参数的不同，代表不同的意思;
 * @param type 这个参数比较复杂
 * 0: 对话框是 sendNext 或者 sendPrev 或者 sendOk 或者 sendNextPrev 生成的。此时 mode 的值 [0 : 点击上一页]; [1 : 点击下一页/确认]; [-1 : 结束对话]
 * 1: 对话框是 sendYesNo 生成的。此时 mode 的值 [0 : 选择否]; [1 : 选择是]; [-1 : 结束对话]
 * 3: 对话框是 sendGetNumber 生成的。此时 mode 的值 [0 : 结束]; [1 : 确定]; [-1 : 结束对话]
 * 4: 对话框是 sendSimple 生成的。此时 mode 的值 [0 : 结束]; [1 : 选择选项]; [-1 : 结束对话]
 * 12: 对话框是 sendAcceptDecline 生成的。此时 mode 的值 [0 : 拒绝]; [1 : 同意]; [-1 : 结束对话]
 * @param selection 对话框上的的选项的索引，从 0 开始。结束对话时，值为 -1
 */
function action(mode, type, selection) {
    // mode 值为 1，表示对话框做了积极的选择，比如点击了【下一页】、【确定】等等
    if (mode === 1) {
        // 只要有积极的选择，说明玩家要进入下一步的操作
        step++;
    } else if (mode === 0) {
        // mode 值为 0，表示对话框做了消极的选择，比如点击了【上一页】、【取消】等等
        // 尝试返回上一步操作
        step--;
    }
    if (mode < 0 || end || step <= 0) {
        // 结束对话释放资源
        cm.dispose();
        return;
    }
    // 第一步：打开一个帮助对话框，引导玩家操作
    if (step === 1) {
        // 检查玩家身上是否携带了百宝券
        var haveTicket = cm.haveItem(ticketId);
        message = "欢迎来到 " + curMapName + " 的快乐百宝箱。请问有什么需要帮忙的么？\r\n\r\n" +
            "#L0#什么是百宝券?#l\r\n" +
            "#L1#哪里可以获得百宝券?#l";
        if (haveTicket) {
            message += "\r\n#L2##b我要使用百宝券抽奖！！！#l#k";
        }
        cm.sendSimple(message);
    } else if (step === 2) {
        // 第二步：根据玩家的选择，做不同的反馈
        if (selection === 0) {
            cm.sendPrev("可以获得稀有的卷轴、设备、椅子、精通书籍和其他很酷的道具!你所需要的只是一张 #b快乐百宝券#k 就可以参与抽奖！");
        } else if (selection === 1) {
            cm.sendPrev("联系#r管理员#k购买，#r联系管理员 #k，。");
        } else if (selection === 2) {
            message = "请选择你要抽奖的形式\r\n\r\n" +
                "#L0##d单抽出奇迹#l\r\n" +
                "#L1##b五连吃保底#l\r\n" +
                "#L2##r十连毁一生#l";
            cm.sendSimple(message);
        } else {
            // 抽奖结束后，如果点击了左下角【结束对话】按钮就会进入这里
            cm.dispose();
        }
    } else if (step === 3) {
        step--;
        if (selection === 0) {
            // 单抽
            luckyDraw(1);
        } else if (selection === 1) {
            // 五连抽
            luckyDraw(5);
        } else if (selection === 2) {
            // 十连抽
            luckyDraw(10);
        } else {
            // 第二步点击了右下角【确定】按钮会进入这里
            cm.dispose();
        }
    } else {
        cm.sendOk("谢谢惠顾!!!")
        cm.dispose();
    }
}

/**
 * 开始抽奖
 * @param num 抽奖次数
 */
function luckyDraw(num) {
    if (inventoryIsFull(num)) {
        cm.sendOk("请检查你的背包\r\n#r装备栏、消耗栏、设置栏、其它栏#k都需要有#b " + num + " #k格空间.");
        cm.dispose();
        return;
    }
    // 检查是否有足够的百宝券
    if (!cm.haveItem(ticketId, num)) {
        cm.sendOk("请检查你的背包。\r\n#r特殊栏#k需要有#b " + num + " #k个百宝券。");
        cm.dispose();
        return;
    }

    // 减少百宝券
    cm.gainItem(ticketId, -num);
    // 开始抽奖
    cm.startGachapon(num);
}

/**
 * 检查背包剩余格子数是否小于指定格子数 num
 * @param num 格子数量
 * @returns true 表示剩余格子数不足 num
 */
function inventoryIsFull(num) {
    return cm.inventoryIsFull(8848, num);
}