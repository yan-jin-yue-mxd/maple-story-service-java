﻿﻿# 项目说明

本项目 fork [HeavenMS](https://github.com/ronancpl/HeavenMS)，是单机冒险岛GMS(国际服)V083的Java服务端。

# 使用说明

1. 创建数据库，例如这里数据库取名为**heavenms**，设置数据库连接账户为：**root**，密码为：**123456**；

2. 导入项目/sql/目录下的几个数据库文件：

   1. 这里要先导入文件**db_database.sql**；
   2. 再导入**db_drops.sql**和**db_shopupdate.sql**；

3. 修改项目配置文件/config.yaml中的数据库连接信息：

   ```
   # 这里指定要连接的数据库主机[localhost]端口[3306]和数据库名称：[heavenms]
   DB_URL: "jdbc:mysql://localhost:3306/heavenms"
   
   # 这个是数据库连接账号
   DB_USER: "root"
   
   # 这个是数据库连接密码
   DB_PASS: "123456"
   
   DB_CONNECTION_POOL: true        #Installs a connection pool to hub DB connections. Set false to default.
   ```

4. 启动服务器：

   1. 使用IDE打开项目，比如我使用的是IDEA；
   2. 等待IDEA自动配置项目。
   3. 进入入口文件：src/net/server/Server.java
   4. 右键点击运行Server.main，也可以使用快捷键Ctrl+Shift+F10

5. 服务器启动成功后会在控制台显示：

   ```
   HeavenMS v83 starting up.
   ...
   Listening on port 8484
   HeavenMS is now online.
   ```

   

# 项目目录说明

## cores
这个目录下存放一些用到的jar包

## docs
这个目录下是收集的一些文档

## handbook
这个目录下的是各类码的说明

## scripts
服务端脚本文件

## sql
这个目录下存放的数据库文件
- db_database.sql: 这个主要是初始化数据库表结构和一些基本数据
- db_drops.sql: 这个主要是初始化掉落数据
- db_shopupdate.sql: 这个主要是初始化商店数据

## src
这个是服务端源码源码目录
### 主要文件说明
- net.server.Server: 入口文件

## tools
这个目录下存放一些工具

## wz
这个是使用工具(如: HaRepacker)将客户端wz资源文件导出的 xml 资源文件.


# 工具软件说明
- [STREDIT](http://www.craftnet.nl/Downloads/):一个可以编辑客户端字符串的工具，当然它还有其他的作用，请自行参考官方。

- HaRepacker：一个可以编辑资源文件并且可以重新打包的工具；
- [HaSuite](https://github.com/hadeutscher/HaSuite)：HaRepacker的作者开发的一个工具套件，里面包含了HaRepacker；
- [Harepacker-resurrected](https://github.com/YanjinTian/Harepacker-resurrected)：是对HaRepacker的一个扩展版。